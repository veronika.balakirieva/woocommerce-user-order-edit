<?php /*
Plugin Name: Woocommerce user account order edit
Plugin URI:
Description: Woocommerce user account order edit.
Author: 
Author URI: 
Text Domain: wuoe
Domain Path: /languages/
Version: 1.0.2
*/

define('WUOE_DIR', plugin_dir_path(__FILE__));

define('WUOE_PLUGIN', __FILE__ );
define('WUOE_PLUGIN_BASENAME', plugin_basename( WUOE_PLUGIN ) );
define('WUOE_PLUGIN_NAME', trim( dirname( WUOE_PLUGIN_BASENAME ), '/' ) );

define('MAX_DATE_UPDATE', 20);
define('EDIT_ORDER_URL', 'edit-order');

/*
 * Create page wuoe-edit
 */
define('CREATE_PAGE_WUOE_EDIT', 0);

function wuoe_load(){
    if(is_admin()){
        require_once(WUOE_DIR.'includes/admin/admin.php');
    }
}
wuoe_load();

if ( CREATE_PAGE_WUOE_EDIT == 1 ) {
    register_activation_hook(__FILE__, 'wuoe_activation');
    function wuoe_activation() {
        global $wpdb;

        $wuoe = $wpdb->get_results("SELECT ID FROM wp_posts WHERE post_name = 'wuoe-edit'");
        if ( empty($wuoe) ){
            $wpdb->insert(
                'wp_posts',
                array(
                    'ping_status' => 'publish',
                    'post_author' => 1,
                    'post_date' => date("Y-m-d H:I:s"),
                    'post_type' => 'page',
                    'post_title' => 'My account order edit',
                    'post_name' => 'wuoe-edit',
                    'post_content' => '[wuoe-edit]',
                )
            );
        }
    }
}

register_uninstall_hook(__FILE__, 'wuoe_uninstall');
function wuoe_uninstall(){}

require_once plugin_dir_path(__FILE__) . 'includes/wuoe-functions.php';

/*
 * Include JS
 */
function theme_js() {
    $curr_page_url = wp_parse_url( $_SERVER['REQUEST_URI'] );
    $curr_page_url = explode('/', $curr_page_url['path']);

    if ( is_page('wuoe-edit') || $curr_page_url[2] == EDIT_ORDER_URL ) {
        wp_enqueue_script('wuoe_js', plugin_dir_url( __FILE__ ) . 'assets/js/wuoe.js', array('jquery'), '1.2', true);
    }

    if ( is_product() ){
        wp_enqueue_script('wuoe_js', plugin_dir_url( __FILE__ ) . 'assets/js/prod.js', array('jquery'), '1.2', true);
    }
}
add_action('wp_enqueue_scripts', 'theme_js');

function add_premium_product(){
    require_once(WUOE_DIR.'includes/add-premium-product.php');
}
add_premium_product();

function wuoe_edit_order_link(){
    require_once(WUOE_DIR.'includes/edit-order-link.php');
}
wuoe_edit_order_link();


/**
 *  Add a custom email to the list of emails WooCommerce should load
 *
 * @since 0.1
 * @param array $email_classes available email classes
 * @return array filtered available email classes
 */
function add_expedited_order_woocommerce_email( $email_classes ) {

	// include our custom email class
	require_once( WUOE_DIR.'includes/class-wc-expedited-order-email.php' );

	// add the email class to the list of email classes that WooCommerce loads
	$email_classes['WC_Expedited_Order_Email'] = new WC_Expedited_Order_Email();

	return $email_classes;

}
add_filter( 'woocommerce_email_classes', 'add_expedited_order_woocommerce_email' );



function add_expedited_factura_woocommerce_email( $email_classes ) {

	// include our custom email class
	require_once( WUOE_DIR.'includes/class-wc-expedited-factura-email.php' );

	// add the email class to the list of email classes that WooCommerce loads
	$email_classes['WC_Expedited_Factura_Order_Email'] = new WC_Expedited_Factura_Order_Email();

	return $email_classes;

}
add_filter( 'woocommerce_email_classes', 'add_expedited_factura_woocommerce_email' );



function wc_get_email_order_items_my( $order, $args = array() ) {
    ob_start();

    $defaults = array(
        'show_sku'      => false,
        'show_image'    => false,
        'image_size'    => array( 32, 32 ),
        'plain_text'    => false,
        'sent_to_admin' => false,
    );

    $args     = wp_parse_args( $args, $defaults );
    $template = $args['plain_text'] ? 'emails/plain/email-order-items-wuore.php' : '/emails/email-order-items-wuore.php';

    wc_get_template(
        $template,
        apply_filters(
            'woocommerce_email_order_items_args',
            array(
                'order'               => $order,
                'items'               => $order->get_items(),
                'show_download_links' => $order->is_download_permitted() && ! $args['sent_to_admin'],
                'show_sku'            => $args['show_sku'],
                'show_purchase_note'  => $order->is_paid() && ! $args['sent_to_admin'],
                'show_image'          => $args['show_image'],
                'image_size'          => $args['image_size'],
                'plain_text'          => $args['plain_text'],
                'sent_to_admin'       => $args['sent_to_admin'],
            )
        )
    );

    return apply_filters( 'woocommerce_email_order_items_table', ob_get_clean(), $order );
}

function wc_get_order_variations_my( $order, $args = array() ) {
    ob_start();

    $defaults = array(
        'show_sku'      => false,
        'show_image'    => false,
        'image_size'    => array( 32, 32 ),
        'plain_text'    => false,
        'sent_to_admin' => false,
    );

    $items = $order->get_items();
    $num = 0;
    foreach ( $items as $item_id => $item ) : $num++;
        if ( $num == 1 ) {
            $product = $item->get_product();

            if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
                continue;
            }
            $orderProductVariation = array();
            foreach ( $item->get_formatted_meta_data() as $meta_id => $meta ) {
            		$value     = $args['autop'] ? wp_kses_post( $meta->display_value ) : wp_kses_post( make_clickable( trim( $meta->display_value ) ) );
            		$strings[] = $args['label_before'] . wp_kses_post( $meta->display_key ) . $args['label_after'] . $value;

                $orderProductVariation[$meta_id] = [
                  'variation_name' => $meta->display_key,
                  'variation_value' => strip_tags($value)
                ];
            	}
            ?>

        <?php }
    endforeach;

    return [apply_filters( 'woocommerce_email_order_items_table', ob_get_clean(), $order ), $orderProductVariation];
}


add_action('woocommerce_order_status_changed', 'send_custom_email_notifications', 10, 4 );
function send_custom_email_notifications( $order_id, $old_status, $new_status, $order ){
    if ( $new_status == 'cancelled' || $new_status == 'failed' ){
        $wc_emails = WC()->mailer()->get_emails(); // Get all WC_emails objects instances
        $customer_email = $order->get_billing_email(); // The customer email
    }

    if ( $new_status == 'cancelled' ) {
        // change the recipient of this instance
        $wc_emails['WC_Email_Cancelled_Order']->settings['heading'] = 'Boostbox medlemskap nr '.$order_id.' er avlyst';
        $wc_emails['WC_Email_Cancelled_Order']->settings['subject'] = 'Boostbox medlemskap nr '.$order_id.' er avlyst';
        $wc_emails['WC_Email_Cancelled_Order']->template_html = 'emails/customer-cancelled-order.php';
        $wc_emails['WC_Email_Cancelled_Order']->recipient = $customer_email;
        // Sending the email from this instance
        $wc_emails['WC_Email_Cancelled_Order']->trigger( $order_id );
    }
    elseif ( $new_status == 'failed' ) {
        // change the recipient of this instance
        $wc_emails['WC_Email_Failed_Order']->recipient = $customer_email;
        // Sending the email from this instance
        $wc_emails['WC_Email_Failed_Order']->trigger( $order_id );
    }
}
