<div class="woocommerce-edit-oder-wrapper">
    <div class="title">
        <?php _e('Velg størrelsen på BOOSTBOX klærne dine '.$order_id, 'woocommerce'); ?>
    </div>
    <div class="sub-title"><?php _e('Du kan gjøre endringer her når du vil.', 'woocommerce'); ?></div>
    <div class="sub-title"><?php _e('Endringer for din neste Box kan bare gjøres mellom den 1. og den 20. denne måneden', 'woocommerce'); ?></div>

    <div class="woocommerce-edit-oder">
        <section class="woocommerce-order-details">
            <?php
            $order_id = wp_parse_url( $_SERVER['REQUEST_URI'] );
            $order_id = explode('/', $order_id['path']);
            $order_id = $order_id[3];
            if ( !is_numeric($order_id) ){
                exit;
            } ?>

            <?php $current_user_id = get_current_user_id();

            $order = wc_get_order( $order_id );
            $order_user_id = $order->get_user_id();

            if ( $current_user_id == $order_user_id ) {
                global $wpdb;

                $order_item_ids = $wpdb->get_results("SELECT order_item_id, order_item_name FROM wp_woocommerce_order_items WHERE order_id = $order_id AND order_item_type='line_item'");

                /* subscription */
                $post_id = $wpdb->get_col("SELECT id FROM wp_posts WHERE post_parent = $order_id");

                $post_ids = '';
                foreach ($post_id as $post_id_1) {
                    $post_ids .= $post_id_1 . ',';
                }
                $post_ids = mb_substr($post_ids, 0, -1);

                $subscription_item_ids = $wpdb->get_col("SELECT order_item_id FROM wp_woocommerce_order_items WHERE order_id IN ($post_ids) AND order_item_type='line_item'");
                $subscription_item_ids_array = $subscription_item_ids;

                $subscriptionItemIds = '';
                foreach ($subscription_item_ids as $subscriptionItemId) {
                    $subscriptionItemIds .= $subscriptionItemId . ',';
                }
                $subscriptionItemIds = mb_substr($subscriptionItemIds, 0, -1);

                $orderItemIds = '';
                foreach ($order_item_ids as $orderItemId) {
                    $orderItemIds .= $orderItemId->order_item_id . ',';
                }
                $orderItemIds = mb_substr($orderItemIds, 0, -1);

                $wuo = $wpdb->get_results("SELECT * FROM wp_woocommerce_order_itemmeta WHERE order_item_id IN ($orderItemIds) ORDER BY meta_id");
                $attribute_taxonomies = $wpdb->get_results("SELECT * FROM wp_woocommerce_attribute_taxonomies ORDER BY attribute_id");

                function sTax($tax, $val) {
                    foreach ($tax as $ss) {
                        foreach ($ss as $s) {
                            if ($s == $val) {
                                $attribute_label = array(
                                    "attribute_name" => $ss->attribute_name,
                                    "attribute_label" => $ss->attribute_label
                                );
                            }
                        }
                    }
                    return $attribute_label;
                }

                $formNum = 0;
                foreach ($order_item_ids as $key => $order_item_id) {
                    $formNum++ ?>
                    <form id="form-wuoe-<?php echo $order_item_id->order_item_id; ?>" class="form-wuoe form-num-<?php echo $formNum;?>" method="post">
                        <?php foreach ($wuo as $row) {
                            if ($order_item_id->order_item_id == $row->order_item_id) {
                                if ($row->meta_key != 'pa_premium') { ?>
                                    <?php $val = explode("_", $row->meta_key);
                                    $val = $val[1];
                                    $aName = sTax($attribute_taxonomies, $val);

                                    if (!empty($aName)) {
                                        $trem_ids = $wpdb->get_results("SELECT term_id FROM wp_term_taxonomy WHERE taxonomy = '$row->meta_key'");
                                        $ids = '';
                                        foreach ($trem_ids as $trem_id) {
                                            $ids .= $trem_id->term_id . ',';
                                        }
                                        $ids = mb_substr($ids, 0, -1);

                                        $terms = $wpdb->get_results("SELECT `name`, `slug` FROM wp_terms WHERE term_id IN ($ids)"); ?>

                                        <div class="tr tr-<?php echo mb_strtolower(preg_replace('/[^a-zA-Z0-9]/', '-', $aName['attribute_label'] )); ?>">
                                            <div class="attribute_label"><?php echo $aName['attribute_label']; ?></div>
                                            <div class="ico ico-<?php echo mb_strtolower(preg_replace('/[^a-zA-Z0-9]/', '-', $aName['attribute_label'] )); ?>"></div>
                                            <div>
                                                <div class="form_toggle">
                                                    <?php $num = 0; foreach ($terms as $term) { $num++;?>
                                                        <?php $disabled = false;
                                                        if ( ($row->meta_key == 'pa_kjonn') and ($row->meta_value != 'ingen') ){ $disabled = true; } ?>
                                                        <?php if ( ($term->slug != 'ingen') || ($term->slug == 'ingen' && $row->meta_key == 'pa_sports-bh') ) { ?>
                                                        <div class="<?php if ( $disabled == true ) echo 'disabled ';?>form_toggle-item value-<?php echo $term->slug; ?> name-<?php echo $row->meta_key; ?>">
                                                            <input <?php echo ($term->slug == $row->meta_value) ? ' checked' : '' ?>
                                                                    <?php if ( $disabled == true ) echo ' disabled ';?>
                                                                    type="radio"
                                                                    name="<?php echo $row->meta_key; ?>"
                                                                    value="<?php echo $term->slug; ?>"
                                                                    data-id="id-<?php echo $aName['attribute_label'];?>-<?php echo $num; ?>"
                                                                    id="id-<?php echo $aName['attribute_label'];?>-<?php echo $formNum;?>-<?php echo $num; ?>"
                                                            >
                                                            <label for="id-<?php echo $aName['attribute_label'];?>-<?php echo $formNum;?>-<?php echo $num; ?>"
                                                                   data-val="<?php if($term->slug=='mann') echo 'mann';?><?php if($term->slug=='kvinne') echo 'kvinne';?>"
                                                                    <?php if ( $disabled == true ) echo ' data-disabled="disabled" ';?>>
                                                                <?php echo $term->name; ?>
                                                            </label>
                                                        </div>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php }
                            }
                        } ?>
                        <div>
                            <div>
                                <?php if ( get_current_user_id() == 1 ) {?>
                                <input type="hidden" value="nomail" name="nomail">
                                <?php } ?>
                                <input type="hidden" value="<?php echo $order_item_id->order_item_id; ?>,<?php echo $subscription_item_ids_array[$key];?>" name="order_item_id">
                                <input type="hidden" value="<?php echo $order_id;?>" name="edit_order">
                                <?php if ( $order->payment_method_title == 'Faktura') { ?>
                                <input type="hidden" value="<?php echo $order->payment_method_title;?>" name="payment-method">
                                <?php }?>
                            </div>
                        </div>
                    </form>
                <?php } ?>

                <button id="save" data-id="form-wuoe-<?php echo $order_item_id->order_item_id;?>" class="submit"><?php _e('Save', 'woocommerce'); ?></button>

            <?php } ?>
        </section>
    </div>
</div>
