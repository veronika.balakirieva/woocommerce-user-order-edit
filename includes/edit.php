<?php
$current_user_id = get_current_user_id();

$order_id = $_REQUEST['id'];
$order = wc_get_order( $order_id );
$order_user_id = $order->get_user_id();

if ($current_user_id == $order_user_id) {
    global $wpdb;

    $order_item_ids = $wpdb->get_col("SELECT order_item_id FROM wp_woocommerce_order_items WHERE order_id = $order_id AND order_item_type='line_item'");

    // subscription
    $post_id = $wpdb->get_col("SELECT id FROM wp_posts WHERE post_parent = $order_id");
    $post_id = $post_id[0];
    $subscription_item_ids = $wpdb->get_col("SELECT order_item_id FROM wp_woocommerce_order_items WHERE order_id = $post_id AND order_item_type='line_item'");
    $subscription_item_ids_array = $subscription_item_ids;
    $subscriptionItemIds = '';
    foreach ($subscription_item_ids as $subscriptionItemId) {
        $subscriptionItemIds .= $subscriptionItemId . ',';
    }
    $subscriptionItemIds = mb_substr($subscriptionItemIds, 0, -1);
    // end subscription

    $orderItemIds = '';
    foreach ($order_item_ids as $orderItemId) {
        $orderItemIds .= $orderItemId . ',';
    }
    $orderItemIds = mb_substr($orderItemIds, 0, -1);

    $wuo = $wpdb->get_results("SELECT * FROM wp_woocommerce_order_itemmeta WHERE order_item_id IN ($orderItemIds)");

    $attribute_taxonomies = $wpdb->get_results("SELECT * FROM wp_woocommerce_attribute_taxonomies");

    function sTax($tax, $val) {
        foreach ($tax as $ss) {
            foreach ($ss as $s) {
                if ($s == $val) {
                    $attribute_label = array(
                        "attribute_name" => $ss->attribute_name,
                        "attribute_label" => $ss->attribute_label
                    );
                }
            }
        }
        return $attribute_label;
    }
    foreach ($order_item_ids as $key => $order_item_id) { ?>
        <form id="form-wuoe-<?php echo $order_item_id; ?>" class="form-wuoe" method="post" style="margin-bottom: 60px;">
            <table>
                <?php foreach ($wuo as $row) {
                    if ($order_item_id == $row->order_item_id) { ?>
                        <?php $val = explode("_", $row->meta_key);
                        $val = $val[1];
                        $aName = sTax($attribute_taxonomies, $val);

                        if (!empty($aName)) {
                            $trem_ids = $wpdb->get_results("SELECT term_id FROM wp_term_taxonomy WHERE taxonomy = '$row->meta_key'");
                            $ids = '';
                            foreach ($trem_ids as $trem_id) {
                                $ids .= $trem_id->term_id . ',';
                            }
                            $ids = mb_substr($ids, 0, -1);

                            $terms = $wpdb->get_results("SELECT `name`, `slug` FROM wp_terms WHERE term_id IN ($ids)"); ?>
                            <tr>
                                <td>
                                    <?php echo $aName['attribute_label']; ?>
                                </td>
                                <td>
                                    <select name="<?php echo $row->meta_key; ?>" style="display:block;">
                                    <?php foreach ($terms as $term) { ?>
                                    <option <?php echo (date('d') >20)?' disabled':'';?> value="<?php echo $term->slug; ?>" <?php echo ($term->slug == $row->meta_value) ? 'selected' : '' ?>><?php echo $term->name; ?></option>
                                    <?php } ?>
                                    </select>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php }
                } ?>
                <tr>
                    <td style="text-align:right;">
                        <?php if ( date('d') > 20 ){?>
                            You can edit from 1st to 20th
                        <?php }?>
                    </td>
                    <td>
                        <input type="hidden" value="<?php echo $order_item_id; ?>,<?php echo $subscription_item_ids_array[$key];?>" name="order_item_id">
                        <button  <?php echo (date('d') >20)?' disabled':'';?> id="fs_plugin_save_button-<?php echo $order_item_id; ?>" class="submit">Save</button>
                    </td>
                </tr>
            </table>
        </form>
    <?php }
} ?>
