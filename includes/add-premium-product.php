<?php
function add_product_to_cart(){
    if ( ! is_admin() ) {
        global $woocommerce;
        $product_premium_id = 339;
        $found = false;
        if ( sizeof($woocommerce->cart->get_cart()) > 0 ) {
            foreach ($woocommerce->cart->get_cart() as $cart_item_key => $values) {
                $_product_id = $values['product_id'];
                $_product_quantity = $values['quantity'];
                $_product_variations = $values['variation'];
                $_product_premium_ja = $_product_variations['attribute_pa_premium'];
                if ( $_product_id == $product_premium_id ){
                    $found = true;
                }
            }
            if (
                !$found && $values['variation_id'] > 0
                //&& $_product_premium_ja == 'ja'
            ) {
                function get_product_premium_variation_id($product_premium_id){
                    $product_obj = new WC_Product_Factory();
                    $product = $product_obj->get_product($product_premium_id);
                    if ($product->product_type == 'variable-subscription') {
                        $children = $product->get_children($args = '', $output = OBJECT);
                        return $children[0];
                    }
                }
                $product_premium_variation_id = get_product_premium_variation_id($product_premium_id);

                $woocommerce->cart->add_to_cart(
                    $product_id = $product_premium_id,
                    $quantity = $_product_quantity,
                    $variation_id = $product_premium_variation_id,
                    $variation = $_product_variations,
                    $cart_item_data = array()
                );
            }
        }
    }
}
