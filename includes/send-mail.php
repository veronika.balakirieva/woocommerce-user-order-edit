<?php
if (isset($_POST)) {
    if (isset($_POST['order_item_id']) && !empty($_POST['order_item_id'])) {

        $path = preg_replace('/wp-content.*$/', '', __DIR__);
        include($path . 'wp-load.php');
        global $wpdb;

        $heading = 'Din ordre nr ' . $_POST['edit_order'] . ' er oppdatert';
        $subject = 'Din ordre nr ' . $_POST['edit_order'] . ' er oppdatert';

        $mailer = WC()->mailer();
        $mails = $mailer->get_emails();
        if (!empty($mails)) {
            foreach ($mails as $mail) {
                if ($mail->id == 'wc_expedited_order') {
                    /* send admin mail */
                    $mail->trigger($_POST['edit_order']);

                    /* send customer mail */
                    $mail->heading = $heading;
                    $mail->settings['heading'] = $heading;
                    $mail->subject = $subject;
                    $mail->settings['subject'] = $subject;
                    $mail->trigger_user($_POST['edit_order']);
                }
                if (
                    $mail->id == 'wc_expedited_factura_order' &&
                    isset($_POST['payment-method']) &&
                    $_POST['payment-method'] == 'Faktura'
                ) {

                    $filenameOder = $_POST['edit_order'];
                    $order = wc_get_order($filenameOder);

                    $response = array();
                    $orderToVo = array();
                    $orderInfoToVo = array();

                    $fileTXT = plugin_dir_path(__FILE__) . "orders/" . $filenameOder . ".txt";
                    $fileJSON = plugin_dir_path(__FILE__) . "orders/" . $filenameOder . ".json";

                    //order info for JSON
                    $orderInfoToVo = array(
                        'navn' => $order->billing_first_name . ' ' . $order->billing_last_name,
                        'address' => $order->billing_address_1 . ' ' . $order->billing_address_2,
                        'postcode' => $order->billing_postcode,
                        'city' => $order->billing_city,
                        'phone' => $order->billing_phone,
                        'dato' => $order->billing_dato
                    );


                    //order info for TXT
                    $text = $order->billing_first_name . ' ' . $order->billing_last_name . ";\r\n";
                    $text .= $order->billing_address_1 . ' ' . $order->billing_address_2 . ";\r\n";
                    $text .= $order->billing_postcode . ' ' . $order->billing_city . ";\r\n";
                    $text .= $order->billing_phone . ";\r\n";
                    $text .= $order->billing_dato . ";\r\n" . "\r\n";

                    $orderProductVariation = wc_get_order_variations_my($order);
                    foreach ($orderProductVariation[1] as $value){
                        $text .= $value['variation_name'].': '.$value['variation_value']. ";\r\n";

                        $orderToVo[] = array('variation_name'=> $value['variation_name'], 'variation_value'=> $value['variation_value']);
                    }


                    if (is_file($fileTXT)){
                        unlink($fileTXT);
                    }
                    // open or create file TXT
                    $fp = fopen($fileTXT, "w");
                    // write to file
                    fwrite($fp, $text);
                    // close
                    fclose($fp);


                    if (is_file($fileJSON)){
                        unlink($fileJSON);
                    }
                    // open or create file JSON
                    $response['info'] = $orderInfoToVo;
                    $response['variations'] = $orderToVo;

                    $fp = fopen($fileJSON, "w");
                    fwrite($fp, json_encode($response));
                    fclose($fp);

                    $attachments = array($fileJSON);
                    $headers = 'From: BoostBox <noreply@boostbox.no>' . "\r\n";
                    $sendTo = $mail->get_option( 'recipient' );
                    wp_mail($sendTo, 'Order #'.$filenameOder.'.json', 'Order #'.$filenameOder.'.json', $headers, $attachments);
                }
            }
        }

        echo json_encode(array('success' => 1, 'order_item_id' => $_POST['order_item_id'], 'dir' => $filenameOder));
    } else {
        echo json_encode(array('success' => 'no order item id'));
    }
} else {
    echo json_encode(array('success' => 0));
}
