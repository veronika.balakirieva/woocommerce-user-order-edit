<?php
function wuoe_admin_page_states( $state, $post) {
    if ( 'page' != $post->post_type ) {
        return $state;
    }
    $pattern = '/\[(wuoe[\w\-\_]+).+\]/';
    preg_match_all ( $pattern , $post->post_content, $matches);
    $matches = array_unique( $matches[0] );
    if ( !empty( $matches ) ) {
        $page      = '';
        $shortcode = $matches[0];
        if ( '[wuoe-edit]' == $shortcode ) {
            $page = 'My account order edit';
        }

        if ( ! empty( $page )) {
            $state['wuoe'] = $page;
        }
    }
    return $state;
}
add_filter( 'display_post_states', 'wuoe_admin_page_states', 10, 2 );