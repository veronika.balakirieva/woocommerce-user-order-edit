<?php
if (isset($_POST)) {
    if ( isset($_POST['order_item_id']) && !empty($_POST['order_item_id']) ){

        $path = preg_replace('/wp-content.*$/','',__DIR__);
        include($path.'wp-load.php');
        global $wpdb;

        $ids = explode(',',$_POST['order_item_id']);
        foreach ( $_POST as $key => $value){
            $wpdb->update(
                'wp_woocommerce_order_itemmeta',
                array( 'meta_value' => $value ),
                array(
                    'order_item_id' => $ids[0],
                    'meta_key' => $key)
            );
        }

        foreach ( $_POST as $key => $value){
            $wpdb->update(
                'wp_woocommerce_order_itemmeta',
                array( 'meta_value' => $value ),
                array(
                    'order_item_id' => $ids[1],
                    'meta_key' => $key)
            );
            $vals = $key;
        }

        echo json_encode(array('success' => 1, 'order_item_id' => $_POST['order_item_id'], 'post'=>$_POST, 'dir'=>plugin_dir_path( __FILE__ )."orders"   ));
    } else {
        echo json_encode(array('success' => 'no order item id'));
    }
} else {
    echo json_encode(array('success' => 0));
}
