<?php
/**
 * Add endpoint
 */
function wuoe_add_my_account_endpoint() {
    add_rewrite_endpoint( EDIT_ORDER_URL, EP_PAGES );
}
add_action( 'init', 'wuoe_add_my_account_endpoint' );

/**
 * Edit page
 */
function wuoe_edit_order_endpoint_content() {
    require_once(WUOE_DIR.'woocommerce/myaccount/edit-order.php');
}
add_action( 'woocommerce_account_'.EDIT_ORDER_URL.'_endpoint', 'wuoe_edit_order_endpoint_content' );


add_filter( 'woocommerce_my_account_my_orders_actions', 'my_account_edit_order_link', 10, 2 );
function my_account_edit_order_link( $actions, $order ) {
    $data = $order->get_data();

    if ( $data['status'] != 'cancelled' ) {
        $edit_order_url = apply_filters( 'woocommerce_get_view_order_url', wc_get_endpoint_url( EDIT_ORDER_URL, $order->data['id'], wc_get_page_permalink( 'myaccount' ) ) );
        $actions['edit'] = array(
            'url'  => $edit_order_url,
            'name' => 'Endre'
        );
    }

    return apply_filters( 'wuoe_edit_order_myaccount_actions', $actions, $order );
}


add_filter( 'woocommerce_valid_order_statuses_for_cancel', 'custom_valid_order_statuses_for_cancel', 10, 2 );
function custom_valid_order_statuses_for_cancel( $statuses, $order ){

    // Set HERE the order statuses where you want the cancel button to appear
    $custom_statuses    = array( 'completed', 'pending', 'processing', 'on-hold', 'failed' );

    // Set HERE the delay (in days)
    $duration = 3; // 3 days

    // UPDATE: Get the order ID and the WC_Order object
    if( isset($_GET['order_id']))
        $order = wc_get_order( absint( $_GET['order_id'] ) );

    $delay = $duration*24*60*60; // (duration in seconds)
    $date_created_time  = strtotime($order->get_date_created()); // Creation date time stamp
    $date_modified_time = strtotime($order->get_date_modified()); // Modified date time stamp
    $now = strtotime("now"); // Now  time stamp

    // Using Creation date time stamp
    if ( ( $date_created_time + $delay ) >= $now ) return $custom_statuses;
    else return $statuses;
}