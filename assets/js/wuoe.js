jQuery(document).ready(function() {

    jQuery('#save').on('click', function (e) {
        var formId = jQuery(this).attr('data-id');

        jQuery('.form-wuoe').each(function () {
            jQuery.ajax({
                type: "POST",
                url: '/wp-content/plugins/woocommerce-user-order-edit/includes/send.php',
                data: jQuery(this).serialize(),
                beforeSend: function(){
                    jQuery('#save').prop("disabled", true);
                    jQuery('#save').html('Oppdaterer...');
                },
                success: function(response) {
                    var jsonData = JSON.parse(response);
                    if (jsonData.success == "1") {
                        jQuery('#save').html('Oppdatert');
                    } else {
                        alert('Invalid Credentials!');
                    }
                }
            });

        });

        sendMail(formId);
    });


    function sendMail(formId){
        jQuery.ajax({
            type: "POST",
            url: '/wp-content/plugins/woocommerce-user-order-edit/includes/send-mail.php',
            data: jQuery('#'+formId).serialize(),
            success: function(response) {
                var jsonData = JSON.parse(response);
                if (jsonData.success == "1") {
                    function button_text(){
                        jQuery('#save').html('Lagre')
                    }
                    setTimeout(button_text, 2000);
                    location.href = "/thank-you/";

                    console.log(jsonData.order_item_id);
                    console.log("jsonData.dir="+jsonData.dir);
                } else {
                    alert('Invalid Credentials! send mail');
                }
            }
        });
    }


    jQuery('.woocommerce-MyAccount-navigation-link--orders').addClass('is-active');


    $('.form_toggle-item').on('click', function () {
        var inpAttr = $(this).find('input').attr('data-id');
        $('input[data-id="'+inpAttr+'"]').prop('checked', true);
    });

});
