(function ($) {
    "use strict"; // Start of use strict

    function lebe_variations_custom() {
        $('.variations_form select, .fami_variations_form select').each(function () {
            jQuery('[data-value="none"]').css({'display':'none'});
        });
    }
    function lebe_variations_custom_ajax() {
        if ( $('.products .variations_form:not(.fami-active-wc_variation_form)').length ) {
            $('.products .variations_form:not(.fami-active-wc_variation_form)').each(function () {
                $(this).wc_variation_form();
            });
        }
        lebe_variations_custom();
    }
    $(document).on('woocommerce_variation_has_changed wc_variation_form', function () {
        lebe_variations_custom();
        $('.product-item').find('.images .fami-img').removeAttr('data-o_sizes').removeAttr('data-o_srcset').removeAttr('sizes').removeAttr('srcset');
    });
    $(document).ajaxComplete(function (event, xhr, settings) {
        if ( settings.hasOwnProperty('data') ) {
            lebe_variations_custom_ajax();
        }
    });

    $(document).on('click', '.variations_form .change-value', function (e) {
        var _this = $(this),
            _change = _this.data('value');

        if ( _change == 'mann' ) {
            jQuery('[value="none"]').prop('selected', true);
            $('.attribute-pa_sports').parent().parent().css('display','none');
        }
        if ( _change == 'kvinne' ) {
            jQuery('[value="none"]').prop('selected', false);
            $('.attribute-pa_sports').parent().parent().css('display','');
        }
    });

})(jQuery); // End of use strict